module.exports = {
  reactStrictMode: true,
  env: {
    ACCESS_TOKEN: process.env.ACCESS_TOKEN
  },
  images: {
    domains: ['avatars.githubusercontent.com'],
  }

}
